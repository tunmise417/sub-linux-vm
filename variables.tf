terraform {
  experiments = [module_variable_optional_attrs]
}

variable "resource_group_name" {
  type = string
  default = "tumitest-rg"
}

variable "subnet_name" {
  type = string
  default = "Subnet01"
}

variable "virtual_network_name" {
  type = string
  default = "my-vnet"
}

variable "ssh_key" {
  description = "Public SSH Key value to be used for ssh access to the VMs."
  type        = string
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "A map of the tags to use on the resources that are deployed with this module."

  default = {
    source = "terraform"
  }
}

variable "boot_diagnostics" {
  type        = bool
  description = "(Optional) Enable or Disable boot diagnostics."
  default     = true
}

variable "linux_vms" {
   default = {
    vm1 = {
      vm_hostname = "linuxVM01"
      vm_size                       = "Standard_D2s_v3"
      vm_os_id                      = ""
      vm_os_publisher               = "Canonical"
      vm_os_offer                   = "UbuntuServer"
      vm_os_sku                     = "18_04-lts-gen2"
      vm_os_version                 = ""
      vm_os_storage_type          = "Standard_LRS"
      enable_accelerated_networking = true
      enable_ssh_key                = false
      storage_data_disks = [
        {
          id                   = 1
          disk_size_gb         = 32
          storage_type = "Premium_LRS"
         
        },
        {
          id                   = 2
          disk_size_gb         = 64
          storage_type = "Premium_LRS"
        }
      ]
    }
    vm2 = {
      vm_hostname                   = "linuxvm02"
      vm_size                       = "Standard_D2s_v3"
      vm_os_id                      = ""
      vm_os_publisher               = "Canonical"
      vm_os_offer                   = "UbuntuServer"
      vm_os_sku                     = "18_04-lts-gen2"
      vm_os_version                 = "latest"
      vm_os_storage_type          = "Standard_LRS"
      enable_accelerated_networking = true
      enable_ssh_key                = false
    }
    }
}

variable "windows_vms" {
   default = {
    vm1 = {
      vm_hostname = "winvm01"
      vm_size                       = "Standard_B4ms"
      vm_os_id                      = ""
      vm_os_publisher               = "MicrosoftWindowsServer"
      vm_os_offer                   = "WindowsServer"
      vm_os_sku                     = "2019-Datacenter"
      vm_os_version                 = "latest"
      vm_os_storage_type          = "Standard_LRS"
      enable_accelerated_networking = false
      storage_data_disks = [
        {
          id                   = 1
          disk_size_gb         = 32
          storage_type = "Premium_LRS"
         
        },
        {
          id                   = 2
          disk_size_gb         = 64
          storage_type = "Premium_LRS"
        }
      ]
    }
    vm2 = {
      vm_hostname                   = "winvm02"
      vm_size                       = "Standard_B4ms"
      vm_os_id                      = ""
      vm_os_publisher               = "MicrosoftWindowsServer"
      vm_os_offer                   = "WindowsServer"
      vm_os_sku                     = "2019-Datacenter"
      vm_os_version                 = "latest"
      vm_os_storage_type          = "Standard_LRS"
      enable_accelerated_networking = false
    }
   }
}