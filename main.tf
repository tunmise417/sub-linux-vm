module "az-vm" {
    source = "git::https://gitlab.com/tunmise417/az_vm_module2"
    resource_group_name = data.azurerm_resource_group.rg.name
    location = data.azurerm_resource_group.rg.location
    vnet_subnet_id = data.azurerm_subnet.subnet.id
    boot_diagnostics = var.boot_diagnostics
    admin_username = "azureadmin"
    admin_password = "Whyclef@123"
    ssh_key = var.ssh_key
    enable_availability_set = true
    linux_vms = var.linux_vms 
    windows_vms = var.windows_vms
}