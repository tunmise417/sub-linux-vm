output "linux_vm_ids" {
  value = module.az-vm.linux_vm_ids
}

output "windows_vm_ids" {
  value = module.az-vm.windows_vm_ids
}

output "linux_nic_ids" {
  value = module.az-vm.linux_network_interface_ids
}

output "windows_nic_ids" {
  value = module.az-vm.windows_network_interface_ids
}